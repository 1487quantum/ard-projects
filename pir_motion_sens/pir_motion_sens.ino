/*
Arduino PIR Motion Sensor tutorial
http://cyaninfinite.com/tutorials/detecting-motion-using-pir-motion-sensor/
Visit us for more tutorials and projects!
*/

const int ledPin=13;//The led to indicate the motion
 
void setup(){
   Serial.begin(9600);
   pinMode(2, INPUT);//Use pin 2 to receive the signal outputted by the            module 
   pinMode(ledPin, OUTPUT);
   pinMode(6,OUTPUT);
}
 
void loop() {
   int sensorValue = digitalRead(2);
   if(sensorValue==1){
      digitalWrite(ledPin,HIGH);   //For debugging purposes
      digitalWrite(6,HIGH);   //To make the beep sound
      delay(100);
      digitalWrite(6,LOW);
      delay(100);
   }
   else{
      digitalWrite(ledPin,LOW);
      Serial.println(sensorValue, DEC);//Print the state of the signal through the serial monitor.
   }
}