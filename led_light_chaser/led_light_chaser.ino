/*
Arduino LED Light chaser tutorial
http://cyaninfinite.com/tutorials/simple-led-light-chaser/
Visit us for more tutorials and projects!
*/

int potPin = 0; // Input pin for the potentiometer
int delayLED = 100; // default delay for led's (microseconds) 
int pValue = 0;		//To store raw value of potentiometer
int ledPin[] = {9, 10, 11}; 

void setup() { 
for(int i = 0; i<3;i++){
pinMode(ledPin[i], OUTPUT); 
}
 
Serial.begin(9600); //Monitor value of potentiometer
} 

void loop() { 
  pValue = analogRead(potPin);// read the value from potentiometer
  pValue = map(pValue, 0, 1023, 10, 500);   // Map pValue from 0-1023 to 10-500
  delayLED = pValue;             // pValue is used to pause between LEDs (in milliseconds) 
  Serial.println(pValue); 			//Print delay in Serial Monitor
  
  for(int i = 0; i<3;i++){
  digitalWrite(ledPin[i], HIGH);
  delay(delayLED);
  digitalWrite(ledPin[i], LOW);
  delay(delayLED);
  }
}