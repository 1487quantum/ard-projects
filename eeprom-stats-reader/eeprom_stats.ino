#include <SoftwareSerial.h> 
#include <EEPROM.h>

const int btnBPin = 3; // the number of the pushbutton pin (Back Button)
const int btnEPin = 4; // the number of the pushbutton pin (Enter Button)
const int btnUPin = 5; // the number of the pushbutton pin (UP Button)
const int btnDPin = 6; // the number of the pushbutton pin (Down Button)
const int TxPin = 8;
int csr = 0; //location of cursor, menu selection
int disp = 0; //0 for main menu, 1 for display all eeprom value, 2 for writing value
int btnBState = 0;
int btnEState = 0;
int btnUState = 0;
int btnDState = 0;
int lstbtnBState = 0;
int lstbtnEState = 0;
int lstbtnUState = 0;
int lstbtnDState = 0;
int nwAdd = 0; //New address
int nwVal = 0; //New value
int setMode = 0;

SoftwareSerial mySerial = SoftwareSerial(255, TxPin);

void setup() {
    mySerial.begin(9600);
    pinMode(TxPin, OUTPUT);
    digitalWrite(TxPin, HIGH);
    mySerial.write(12);
    delay(5);
    mySerial.println("EEPROM Stats 1.0");
    mySerial.println("Loading...");
    delay(1000);
}

void loop() {
    mySerial.write(12); //Clear Screen
    getBtnVal();
    if (disp == 0) {
        showMenu();
    } else if (disp == 1) {
        showValues();
    } else if (disp == 2) {
        //csr = 3; //Prevent interference with disp
        getBtnVal();
        writeValue();
    }
    delay(100); // delay
}

void getBtnVal() {
    // read the state of the pushbutton value:
    btnBState = digitalRead(btnBPin);
    btnEState = digitalRead(btnEPin);
    btnUState = digitalRead(btnUPin);
    btnDState = digitalRead(btnDPin);

    //For Up

        if (btnUState == HIGH) {
            // if the current state is HIGH then the button
            // wend from off to on:
            if (disp == 0) {
                if (csr == 1) {
                    csr = 0;
                } else {
                    csr += 1;
                }
            }
            else if (disp == 2) {
              if(setMode==1){
                nwAdd += 1;
                if (nwAdd == 255) {
                    nwAdd = 0;
                }
              }else if(setMode==2){
                nwVal += 1;
                if (nwVal == 255) {
                    nwVal = 0;
                }
              } else if(setMode==3){
                //Accept write
        mySerial.write(12); //Clear
        mySerial.print("ADD:");
        mySerial.print(nwAdd);
        mySerial.print(", VAL:");
        mySerial.println(nwVal);
        mySerial.write(13);
        mySerial.println("Writing... Done!");
        EEPROM.write(nwAdd, nwVal);
        delay(2000);
        disp = 0;
        csr = 0;
              }   
            }
        }


    //For Down
    
        if (btnDState == HIGH) {
            if (disp == 0) {
                if (csr == 0) {
                    csr = 1;
                } else {
                    csr -= 1;
                }
            }
            else if (disp == 2) {
              if(setMode == 1){
                nwAdd -= 1;
                if (nwAdd == -1) {
                    nwAdd = 254;
                }
              }else if(setMode == 2){
                    nwVal -= 1;
                    if (nwVal == -1) {
                        nwVal = 254;
                    }
                
              }        
            else if(setMode == 3){
               //Decline write
        mySerial.write(12); //Clear
        mySerial.print("ADD:");
        mySerial.print(nwAdd);
        mySerial.print(", VAL:");
        mySerial.println(nwVal);
        mySerial.write(13);
        mySerial.println("Write Cancelled!");
        delay(2000);
        disp = 0;
        csr = 0; 
              }
        }
        }
    

    //For Enter
    
        if (btnEState == HIGH) {
          if(disp==0){
            if (csr == 0) {
                disp = 1;
            } else if (csr == 1) {
                disp = 2;
            }
          }
           else if(disp==2){
              if (setMode==0){
                setMode = 1;
                 mySerial.write(12); //Clear
              } 
              
              else if (setMode==1){
                setMode = 2;
        mySerial.write(12); //Clear
              } else if (setMode==2){
                setMode = 3;
        mySerial.write(12); //Clear
              }     
              
             
            }
        }
    

    //For Back
    
        if (btnBState == HIGH) {
            if (disp == 2) {
                if (setMode < 0) {
                    setMode = 0;
                }
             else if (setMode == 0) {
                    disp = 0;
                    csr = 0;
                    nwAdd = 0;
                    nwVal = 0;
                }
                setMode -= 1;
            }

        }
  
    delay(10);
}



void showMenu() {
    if (csr == 0) {
        mySerial.println("> READ ALL ");
        mySerial.write(13);
        mySerial.println("  WRITE VAL");
    } else if (csr == 1) {
        mySerial.println("  READ ALL");
        mySerial.write(13);
        mySerial.println("> WRITE VAL ");
    }
}

void showValues() {
    for (int i = 0; i < 512; i++) {
        mySerial.write(12); //Clear
        int value = 0;
        value = EEPROM.read(i);
        mySerial.print("ADD: ");
        mySerial.println(i);
        mySerial.write(13);
        mySerial.print("VAL: ");
        mySerial.println(value);
        delay(50);
    }
    disp = 0;
}

void writeValue() {
  if(disp==2){
    if (setMode == 1) {
        mySerial.print("SET ADD: ");
        mySerial.println(nwAdd);
    } else if (setMode == 2) {
        mySerial.print("ADD:");
        mySerial.println(nwAdd);
        mySerial.write(13);
        mySerial.print("SET VAL:");
        mySerial.println(nwVal);
    } else if (setMode == 3 ) {
        mySerial.print("ADD:");
        mySerial.print(nwAdd);
        mySerial.print(", VAL:");
        mySerial.println(nwVal);
        mySerial.write(13);
        mySerial.println("Confirm write?"); //Up btn for yes, Down btn for no
    }
  }
}
