// Simple strand test for Adafruit Dot Star RGB LED strip.
// This is a basic diagnostic tool, NOT a graphics demo...helps confirm
// correct wiring and tests each pixel's ability to display red, green
// and blue and to forward data down the line.  By limiting the number
// and color of LEDs, it's reasonably safe to power a couple meters off
// the Arduino's 5V pin.  DON'T try that with other code!

#include <Adafruit_DotStar.h>
// Because conditional #includes don't work w/Arduino sketches...
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
//#include <avr/power.h> // ENABLE THIS LINE FOR GEMMA OR TRINKET

#define NUMPIXELS 30 // Number of LEDs in strip

// Here's how to control the LEDs from any two pins:
#define DATAPIN    4
#define CLOCKPIN   5
Adafruit_DotStar strip = Adafruit_DotStar(
                           NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);
// The last parameter is optional -- this is the color data order of the
// DotStar strip, which has changed over time in different production runs.
// Your code just uses R,G,B colors, the library then reassigns as needed.
// Default is DOTSTAR_BRG, so change this if you have an earlier strip.

// Hardware SPI is a little faster, but must be wired to specific pins
// (Arduino Uno = pin 11 for data, 13 for clock, other boards are different).
//Adafruit_DotStar strip = Adafruit_DotStar(NUMPIXELS, DOTSTAR_BRG);

void setup() {
  Serial.begin(115200);

#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  strip.begin(); // Initialize pins for output
  strip.show();  // Turn all LEDs off ASAP
}

uint32_t color = 0x200000;      // Inital color (starts red)
byte rain_len = 5;   //Max 6, else will overflow as another color

void loop() {
  for (int j = 0; j < 3; j++) {
    //Loop the diff colors, RGB
    for (int i = 0; i < strip.numPixels(); i++) {
      //Shift by 2 Bits for another color
      //strip.setPixelColor(i, color >> j * 8);
      rainFade(i, j);
      strip.show();                     // Refresh strip
      Serial.println((color >> j * 8), HEX);  //Debug
      delay(50);
      clearLED(i);
    }
  }
}

void rainFade(int indx, int sft) {
  for (int i = 0; i < rain_len ; i++) {
    strip.setPixelColor(indx + i, (color >> sft * 8) / pow(2, (rain_len - 1) - i) );
  }
}

void clearLED(int c) {
  for (int i = 0; i < rain_len; i++) {
    strip.setPixelColor(c + i, 0); //Off LED
  }
}

